#!/bin/bash
rm -r bin/ obj/
dotnet build --self-contained -r linux-arm --configuration Release
docker buildx build --platform linux/arm/v7 -t yen3k/sesh-remindme:latest --push --file Dockerfile-armv7 .
