FROM mcr.microsoft.com/dotnet/sdk:6.0-jammy-arm64v8

WORKDIR /app

RUN mkdir /var/lib/seshremindme

COPY . .

RUN touch /var/lib/seshremindme/remind_dates.txt
RUN dotnet build --configuration Release

ENTRYPOINT ["bin/Release/net6.0/sesh-remindme"]
