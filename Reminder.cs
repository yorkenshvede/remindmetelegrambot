
namespace remindme{ 
    class Reminder {
        private long chatId;
        private long replyMessageId;
        private DateTime remindDate;
        private string path;

        public Reminder (long chatId, long replyMessageId, DateTime remindDate, string path){
            this.chatId = chatId;
            this.replyMessageId = replyMessageId;
            this.remindDate = remindDate;
            this.path = path;
        }

        public async Task WriteToFile(){
            using StreamWriter sw = new StreamWriter(path, append: true);
            await sw.WriteLineAsync($"{chatId} {replyMessageId} {remindDate.ToString("dd/MM/yyyy HH:mm")}");
        }

        

    }
}