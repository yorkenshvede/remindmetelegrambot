﻿using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using remindme;

string BOTKEY = Environment.GetEnvironmentVariable("BOTKEY")!;
try{
    if (BOTKEY == null) {
        throw new ArgumentNullException("Environment variable \"BOTKEY\" is not set");
    }
} catch (ArgumentNullException e) {
    Console.WriteLine(e.Message);
    Environment.Exit(1);
}

var botClient = new TelegramBotClient(BOTKEY);
using var cts = new CancellationTokenSource();
RemindTimer();

// StartReceiving does not block the caller thread. Receiving is done on the ThreadPool.
var receiverOptions = new ReceiverOptions
{
    AllowedUpdates = Array.Empty<UpdateType>(), // receive all update types
    ThrowPendingUpdates = true,
};
botClient.StartReceiving(
    updateHandler: HandleUpdateAsync,
    pollingErrorHandler: HandlePollingErrorAsync,
    receiverOptions: receiverOptions,
    cancellationToken: cts.Token
);

var me = await botClient.GetMeAsync();
Console.WriteLine($"Start listening for @{me.Username}");
Console.ReadLine();

// Send cancellation request to stop bot
cts.Cancel();

async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
{
    // Only process Message updates: https://core.telegram.org/bots/api#message
    if (update.Message is not { } message){
        return;
    }
    // Check if message is a reply
    if (message.ReplyToMessage is not { } replyMessage){
        return;
    }

    // Only process text messages
    if (message.Text is not { } messageText){
        return;
    }

    var replyDate = replyMessage.Date;
    var replyId = replyMessage.MessageId;
    var chatId = message.Chat.Id;

    DateTime remindDate = new DateTime(0);
    //Write reminder to file
    if(messageText.Split(" ").Length > 0)
        remindDate = GetRemindTime(messageText, replyDate);
        Reminder reminder = new Reminder(chatId, replyId, remindDate, "/var/lib/seshremindme/remind_dates.txt");
        await reminder.WriteToFile();
}

DateTime GetRemindTime(string msgText, DateTime replyDate){
    string[] messageTextArr = msgText.Split(" ");
    replyDate = replyDate.AddHours(2); //korriger for at matche System.Time på vores com
    //Add time to the date and time of original message sent.
    if (messageTextArr.Length > 1){
        for (int i = 1; i < messageTextArr.Length; i++){
            switch (messageTextArr[i][messageTextArr[i].Length -1]) {
                case 'Y':
                    replyDate = replyDate.AddYears(int.Parse(messageTextArr[i].Substring(0, messageTextArr[i].Length -1)));
                    break;
                case 'M':
                    replyDate = replyDate.AddMonths(int.Parse(messageTextArr[i].Substring(0, messageTextArr[i].Length -1)));
                    break;
                case 'D':
                    replyDate = replyDate.AddDays(int.Parse(messageTextArr[i].Substring(0, messageTextArr[i].Length -1)));
                    break;
                case 'h':
                    replyDate = replyDate.AddHours(int.Parse(messageTextArr[i].Substring(0, messageTextArr[i].Length -1)));
                    break;
                case 'm':
                    replyDate = replyDate.AddMinutes(int.Parse(messageTextArr[i].Substring(0, messageTextArr[i].Length -1)));
                    break;
            }
        }

    }
    return replyDate;
}

async void RemindTimer(){
    var timer = new PeriodicTimer(TimeSpan.FromMinutes(1)); //Tick every minute
    while (await timer.WaitForNextTickAsync()){
    //read in file line by line
        foreach (string line in System.IO.File.ReadLines("/var/lib/seshremindme/remind_dates.txt")){
            string[] lineArr = line.Split(" ");
            ChatId chatId = new ChatId(long.Parse(lineArr[0]));
            int messageId = int.Parse(lineArr[1]);
            string remindDateAndTime = lineArr[2] + " " + lineArr[3];
            string nowStr = DateTime.Now.ToString("dd/MM/yyyy HH:mm");

            //If date and time of current lime equals now, send the message.
            if (remindDateAndTime == nowStr) {
                await botClient.ForwardMessageAsync(chatId, chatId, messageId);
            }
        }
    }
}

Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
{
    var ErrorMessage = exception switch
    {
        ApiRequestException apiRequestException
            => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
        _ => exception.ToString()
    };

    Console.WriteLine(ErrorMessage);
    return Task.CompletedTask;
}
